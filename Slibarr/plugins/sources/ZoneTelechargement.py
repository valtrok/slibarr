import re
import cloudscraper
import lxml.html
from lxml.cssselect import CSSSelector

from Slibarr.config import config
from .SourceInterface import SourceInterface


class ZoneTelechargement(SourceInterface):
    name = 'Zone-Téléchargement'
    _URL = config.plugins['sources']['ZoneTelechargement']['URL']
    uri_regex = config.plugins['sources']['ZoneTelechargement']['uri-regex']

    def __init__(self):
        self.session = cloudscraper.create_scraper(delay=10)
        self._data = {
            'do': 'search',
            'subaction': 'search',
            'user_hash': '{user_hash}'
        }
        self.links_cache = {}

    def _get_type(self, tree, selector, movie_type, tvshow_type):
        type_text = selector(tree)[0].text
        for k in ['Animes', 'Séries']:
            if k in type_text:
                return tvshow_type
        if 'Films' in type_text:
            return movie_type
        return None

    def _get(self, url):
        html = self.session.get(url).text
        return lxml.html.fromstring(html)

    def _post(self, url, data):
        html = self.session.post(url, data).text
        return lxml.html.fromstring(html)

    def _format_link(self, link):
        if link[0] == '/':
            return self._URL + link[1:]
        return link

    def _get_entity(self, tree, uri):
        from Slibarr.schema import MovieEntity, Season

        type_sel = CSSSelector(
            '#dle-content > article > div:nth-child(1) > span > strong > a')
        infos_sel = CSSSelector(
            '#dle-content > article > div.cols-mov.clearfix.ignore-select > div > div h2~div'
        )

        item_type = self._get_type(tree, type_sel, MovieEntity, Season)

        infos_text = infos_sel(tree)[0].itertext()
        next(infos_text)
        quality = next(infos_text).replace('Qualité', '').strip()
        next(infos_text)
        language = next(infos_text).strip()

        next(infos_text)
        if item_type == Season:
            n = next(infos_text).strip().split(' ')[1]
            return item_type(
                uri=uri,
                source=self.name,
                quality=quality,
                language=language,
                n=int(n))
        return item_type(
            uri=uri, source=self.name, quality=quality, language=language)

    def _get_all_links(self, tree):
        from Slibarr.schema import Link

        tables_sel = CSSSelector(
            '#dle-content > article > div.cols-mov.clearfix.ignore-select > div > div > div > div.table-responsive'
        )

        hoster_sel = CSSSelector('div.table-responsive tr > th:nth-child(1)')
        link_sel = CSSSelector(
            'div.table-responsive tr > td:nth-child(1) > a.download')

        links = []
        for match in tables_sel(tree):
            hoster = next(hoster_sel(match)[0].itertext()).strip()
            for l in link_sel(match):
                name = l.text.strip()
                link = self._format_link(l.get('href'))
                links.append(Link(name=name, hoster=hoster, link=link))

        return links

    def search(self, query):
        from Slibarr.schema import Movie, TVShow

        item_sel = CSSSelector('#dle-content > div.mov')
        title_sel = CSSSelector('div.mov > a')
        type_sel = CSSSelector('div.mov > div.mov-c')

        self._data.update({'story': query})
        tree = self._post(self._URL, self._data)

        match = item_sel(tree)

        results = []

        for r in match:
            title = title_sel(r)[0]
            item_type = self._get_type(r, type_sel, Movie, TVShow)
            if item_type is not None:
                o = item_type(
                    source=self.name,
                    name=title.get('title'),
                    uri=title.get('href'))
                results.append(o)

        return results

    def get_all_entities(self, uri):
        from Slibarr.schema import MovieEntity, Season

        other_info_sel = CSSSelector(
            '#dle-content > article > header > div.otherversions > a')
        tv_quality_sel = CSSSelector('a > span > span:nth-child(2) > b')
        tv_language_sel = CSSSelector('a > span > span:nth-child(3) > b')
        mo_quality_sel = CSSSelector('a > span > span:nth-child(1) > b')
        mo_language_sel = CSSSelector('a > span > span:nth-child(2) > b')
        n_sel = CSSSelector('a > span > strong > span > b')

        tree = self._get(uri)

        entities = []

        main = self._get_entity(tree, uri)
        if main is not None:
            entities.append(main)

        match = other_info_sel(tree)

        for r in match:
            match_uri = r.get('href')
            if isinstance(main, Season):
                quality = tv_quality_sel(r)[0].text
                language = tv_language_sel(r)[0].text.strip().strip('()')
                n = n_sel(r)[0].text.strip()
                entities.append(
                    Season(
                        uri=match_uri,
                        source=self.name,
                        quality=quality,
                        language=language,
                        n=int(n)))
            else:
                quality = mo_quality_sel(r)[0].text
                language = mo_language_sel(r)[0].text.strip().strip('()')
                entities.append(
                    MovieEntity(
                        uri=match_uri,
                        source=self.name,
                        quality=quality,
                        language=language))
        return entities

    def get_result(self, uri):
        from Slibarr.schema import Movie, TVShow

        title_sel = CSSSelector(
            '#dle-content > article > div.cols-mov.clearfix.ignore-select > div > div > h2 > b'
        )
        type_sel = CSSSelector(
            '#dle-content > article > div:nth-child(1) > span > strong > a')

        tree = self._get(uri)
        name = title_sel(tree)[0].text

        item_type = self._get_type(tree, type_sel, Movie, TVShow)
        return item_type(name=name, uri=uri, source=self.name)

    def get_entity(self, uri):
        tree = self._get(uri)
        return self._get_entity(tree, uri)

    def get_episodes(self, season):
        from Slibarr.schema import Episode

        if season.uri in self.links_cache:
            links = self.links_cache[season.uri].copy()
            del self.links_cache[season.uri]
        else:
            tree = self._get(season.uri)
            links = self._get_all_links(tree)
            self.links_cache[season.uri] = links

        done = []
        episodes = []

        for l in links:
            if 'Episode' in l.name:
                n = re.sub(
                    r'episode\s*(?:final)?', '', l.name, flags=re.I).strip()
                if n not in done:
                    episodes.append(
                        Episode(
                            uri=season.uri,
                            source=self.name,
                            quality=season.quality,
                            language=season.language,
                            n=int(n)))
                    done.append(n)

        return episodes

    def get_links(self, entity):
        from Slibarr.schema import Season, Episode

        if entity.uri in self.links_cache:
            links = self.links_cache[entity.uri].copy()
            del self.links_cache[entity.uri]
        else:
            tree = self._get(entity.uri)
            links = self._get_all_links(tree)
            self.links_cache[entity.uri] = links

        if isinstance(entity, Season):
            return [l for l in links if 'Episode' not in l.name]
        elif isinstance(entity, Episode):
            return [
                l for l in links if re.match(
                    r'Episode\s*(?:final)?\s*' + str(entity.n) + '$',
                    l.name,
                    flags=re.I)
            ]
        return links
