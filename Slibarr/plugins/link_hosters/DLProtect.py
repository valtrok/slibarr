import cloudscraper
import lxml
from lxml.cssselect import CSSSelector

from Slibarr.config import config
from .LinkHosterInterface import LinkHosterInterface


class DLProtect(LinkHosterInterface):
    name = 'DL-Protect'
    uri_regex = config.plugins['link-hosters']['DLProtect']['uri-regex']

    def __init__(self):
        self.session = cloudscraper.create_scraper(delay=10)

    def resolve_url(self, url):
        link_sel = CSSSelector(
            'body > center > div > div.content > div.lienet > a')

        html = self.session.post(url, {'submit': 'Continuer'}).text
        tree = lxml.html.fromstring(html)

        return link_sel(tree)[0].get('href')
